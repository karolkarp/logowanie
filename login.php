<?php

session_start();

require_once 'header.php';
include 'dbconn.php';

function filtr($var) {
   if (get_magic_quotes_gpc())
      $var = stripslashes($var);

   return mysql_real_escape_string(htmlspecialchars(trim($var)));
}

echo <<< _END
      <div class="container">
         <div class="row"> 
            <div class="col-md-12">
               <form class="form-horizontal" method="POST" action="login.php">
                  <div class="form-group">
                     <label for="exampleInputName2" class="col-sm-2 control-label">Login:</label>
                     <div class="col-sm-10">
                        <input type="text" class="form-control" id="exampleInputName2" name="user" placeholder="Jane Doe">
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="inputPassword3" class="col-sm-2 control-label">Password:</label>
                     <div class="col-sm-10">
                        <input type="password" class="form-control" id="inputPassword3" name="password" placeholder="Password">
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default" name="loguj">Zaloguj</button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
            <div class="row">
            <div class="col-md-6 col-md-offset-3">
               <a class="btn btn-info btn-lg btn-block" href="main.php">Powrót</a>
            </div>
         </div>
      </div>
_END;

if ($_SESSION['logged']) {
   echo 'Już jestes zalogowany jako ' . $_SESSION['user'] . "<br>";
   echo '<a href="main.php">Powrót</a> ';
} else {
   if (isset($_POST['loguj'])) {
      $user = filtr($_POST['user']);
      $password = filtr($_POST['password']);

      if (empty($user) || empty($password)) {
         echo "Uzupełnj podane pola";
      } else {
         $user = filtr($user);
         $password = filtr($password);
         $password = md5($password);

         $result = $pdo->query("SELECT * FROM log WHERE user='$user' AND password1='$password'");

         $row = $result->fetch(PDO::FETCH_ASSOC);
         //var_dump($row);
         $a = count($row['id']);
         if ($a == 1) {
            $_SESSION['logged'] = true;
            $_SESSION['id'] = $row['id'];
            $_SESSION['user'] = $row['user'];
            //var_dump($row);

            header("refresh:1;url=". $link2 ."succesfull.php");
            
         }
      }
   }
}
require_once 'footer.php';
