<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Bootstrap</title>

      <link href="css/bootstrap.min.css" rel="stylesheet">

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="js/javascript.js"></script>

      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
      <link href="css/newStyle.css" rel="stylesheet">
   </head>
   <body>
      <nav class=" navbar navbar-default">
         <div class="container-fluid">
            <div class="navbar-header"> <!-- header - dodac glówne logo po lewej oraz button do rozwijania menu w wer. mob -->
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
               </button>
               <a href="main.php" class="navbar-brand">Serwis</a>
            </div>
            <!-- elementy dodane do listy -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <ul class="nav navbar-nav">
                  <li><a href="about.php">O nas</a></li>
                  <li><a href="products.php">Nasze produkty</a></li>
               </ul>
               <ul class="nav navbar-nav navbar-right">
                  <?php
                  if (!@$_SESSION['logged']) {
                     
                  } else {
                     echo "<li><a href='#'><i class='fa fa-shopping-cart' aria-hidden='true'></i> Koszyk</a></li>
                        <li><a href='logout.php'><i class='fa fa-sign-out' aria-hidden='true'></i> Wyloguj</a></li>";
                  }
                  ?>
               </ul>
            </div>
         </div>
      </nav>
      <div class="container">
         <div class="row">
            <div class="col-lg-12 col-xs-12">
               <div class="header">
                  <h1>Witamy w serwisie
                     <?php
                     echo @$_SESSION['user'];
                     ?> !</h1>
               </div>
            </div>
         </div>
      </div>