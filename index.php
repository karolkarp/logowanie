<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Bootstrap</title>

      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link href="css/newStyle.css" rel="stylesheet">

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

   </head>
   <body>
      <nav class=" navbar navbar-default">
         <div class="container-fluid">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
               </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <ul class="nav navbar-nav">
                  <li><a href="about.php">O nas</a></li>
                  <li><a href="products.php">Nasze produkty</a></li>
               </ul>
               <ul class="nav navbar-nav navbar-right">
                  <li><a href="login.php"><i class="fa fa-sign-in fa-1x" aria-hidden="true"></i> Zaloguj się</a></li>
                  <li><a href="register.php"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Zarejestruj się</a></li>
               </ul>
            </div>
         </div>
      </nav>
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-xs-12">
               <div class="header">
                  <h1>Witamy w serwisie
                     <?php
                     session_start();
                     echo $_SESSION['user'];
                     if ($_SESSION['logged']) {
                        header("location: http://localhost/logowanie/main.php");
                     }
                     ?> !
                  </h1>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-lg-3">
               <img src="img/pexels-photo-225968.jpeg" class="img-responsive" alt="Responsive image">
            </div>
            <div class="col-lg-6">
               <div class="row  center-block">

                  <?php
                  require 'dbconn.php';

                  $add_info = $pdo->query("SELECT text FROM log WHERE user = '" . $_SESSION['user'] . "' ");
                  $rowww = $add_info->fetch(PDO::FETCH_ASSOC);

                  $output = $rowww['text'];
                  echo "<h3>Informacje o Twoim profilu: </h3> " . "$output";
                  ?>
               </div>
            </div>
            <div class="col-lg-3 asd">
               <p><h2>Musisz być zalogowany aby zobaczyć oraz dodać informacje o profilu.</h2></p>
            </div>
         </div>
      </div>
      <?php
      include 'footer.php';
      ?>