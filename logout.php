<?php
session_start();
require 'header2.php';
require 'config.php';

if ($_SESSION['logged']) {
   $_SESSION['logged'] = false;
   $_SESSION['user'] = '';
   $_SESSION['id'] = '';

   echo <<< _END
      <div class="container container-table">
          <div class="row vertical-center-row">
              <div class="text-center col-md-4 col-md-offset-4">
                 <h1>Wylogowano</h1>
              </div>
          </div>
      </div>
_END;

   header("refresh:3;url=". $link2 ."main.php");
} else {
      echo <<< _END
      <div class="container container-table">
          <div class="row vertical-center-row">
              <div class="text-center col-md-4 col-md-offset-4">
                 <h1>Musisz się zalogować</h1>
                 <h3>Przekierowanie za 3 sekund</h3>
              </div>
          </div>
      </div>
_END;
   header("refresh:2;url=". $link2 ."main.php");
}