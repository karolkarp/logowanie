<?php
include "header.php";
?>
<div class ="container">
   <div class="row">
      <div class="col-md-6 col-md-offset-3">
         <p><b><i class="fa fa-quote-left fa-3x pull-left fa-border"></i>
               W przeciwieństwie do rozpowszechnionych opinii, <a href="https://pl.wikipedia.org/wiki/Lorem_ipsum" target="_blank" data-placement="top" data-toggle="tooltip" class="dymek" data-original-title="Dokładnie znaczenie w linku."> Lorem Ipsum</a> 
               nie jest tylko przypadkowym tekstem. Ma ono korzenie w klasycznej łacińskiej literaturze z 45 roku przed Chrystusem, czyli ponad 2000 
               lat temu! Richard McClintock, wykładowca łaciny na uniwersytecie Hampden-Sydney w Virginii, przyjrzał się uważniej jednemu z najbardziej 
               niejasnych słów w Lorem Ipsum – consectetur – i po wielu poszukiwaniach odnalazł niezaprzeczalne źródło: Lorem Ipsum pochodzi z fragmentów 
               (1.10.32 i 1.10.33) „de Finibus Bonorum et Malorum”, czyli „O granicy dobra i zła”, napisanej właśnie w 45 p.n.e. przez Cycerona. Jest to bardzo 
               popularna w czasach renesansu rozprawa na temat etyki. Pierwszy wiersz 
               <a href="https://pl.wikipedia.org/wiki/Lorem_ipsum" target="_blank" data-placement="top" data-toggle="tooltip" class="dymek" data-original-title="Dokładnie znaczenie w linku."> Lorem Ipsum</a> ,
               „Lorem ipsum dolor sit amet...” pochodzi właśnie z sekcji 1.10.32.

               Standardowy blok Lorem Ipsum, używany od XV wieku, jest odtworzony niżej dla zainteresowanych. Fragmenty 1.10.32 i 1.10.33 z „de Finibus Bonorum et Malorum” Cycerona, są odtworzone w dokładnej, oryginalnej formie, wraz z angielskimi tłumaczeniami H. Rackhama z 1914 roku.</b></p> 
      </div> 
   </div> 
</div>
<?php
include "footer.php";