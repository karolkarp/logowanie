<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Bootstrap</title>

      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link href="css/newStyle.css" rel="stylesheet">
   </head>
   <body>
      <nav class=" navbar navbar-default">
         <div class="container-fluid">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
               </button>
               <a href="#" class="navbar-brand">Serwis</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <ul class="nav navbar-nav">
                  <li><a href="#">O nas</a></li>
                  <li><a href="#">Nasze produkty</a></li>
               </ul>
               <ul class="nav navbar-nav navbar-right">
                  <?php
                  if (!@$_SESSION['logged']) {
                     
                  } else {
                     echo "<li><a href='#'>Koszyk</a></li>
                     <li><a href='logout.php'>Wyloguj</a></li>";
                  }
                  ?>
               </ul>
            </div>
         </div>
      </nav>
      <div class="container">
         <div class="row">
            <div class="col-lg-12 col-xs-12">
               <div class="header">
                  <h1>Do zobaczenia niebawem
                     <?php
                     echo @$_SESSION['user'];
                     ?> !</h1>
               </div>
            </div>
         </div>
      </div>